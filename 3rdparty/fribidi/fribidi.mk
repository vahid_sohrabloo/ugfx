GFXSRC +=   $(GFXLIB)/3rdparty/fribidi/lib/fribidi-arabic.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-bidi.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-bidi-types.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-deprecated.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-joining.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-joining-types.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-mem.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-mirroring.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-run.c \
            $(GFXLIB)/3rdparty/fribidi/lib/fribidi-shape.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-cap-rtl.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-cp1255.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-cp1256.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-iso8859-6.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-iso8859-8.c \
            $(GFXLIB)/3rdparty/fribidi/charset/fribidi-char-sets-utf8.c

GFXINC += $(GFXLIB)/3rdparty/fribidi/lib $(GFXLIB)/3rdparty/fribidi/charset

 



